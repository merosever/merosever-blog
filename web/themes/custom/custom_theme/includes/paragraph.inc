<?php

/**
 * @file
 * Theme and preprocess functions for paragraphs.
 */

/**
 * Implements hook_preprocess_HOOK() for paragraph-code.html.twig.
 */
function custom_theme_preprocess_paragraph__p_code(array &$variables) {
  $variables['#attached']['library'][] = 'custom_theme/paragraph.code';
}