<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Entity;

/**
 * Hero block for blog_article node type.
 *
 * @CustomHeroEntity(
 *   id = "custom_hero_node_blog_article",
 *   entity_type = "node",
 *   entity_bundle = {"blog_article"}
 * )
 */
class NodeBlogArticle extends CustomHeroEntityPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeroSubtitle() {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getEntity();
    return $node->get('body')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $this->getEntity();
    /** @var \Drupal\media\MediaInterface $media */
    $media = $node->get('field_image')->entity;

    return $media->get('field_media_image')->entity->getFileUri();
  }

}
