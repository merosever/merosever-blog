<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Entity;

use Drupal\custom_hero\Plugin\CustomHero\CustomHeroPluginInterface;

/**
 * Interface CustomHero entity plugin type.
 */
interface CustomHeroEntityPluginInterface extends CustomHeroPluginInterface {

  /**
   * Gets entity type ID.
   *
   * @return string
   *   The entity type ID.
   */
  public function getEntityType();

  /**
   * Gets entity bundles.
   *
   * @return string
   *   The entity type bundles.
   */
  public function getEntityBundle();

  /**
   * Gets current entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object.
   */
  public function getEntity();

}
