<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Entity;

use Drupal\custom_hero\Plugin\CustomHero\CustomHeroPluginBase;

/**
 * The base for CustomHero entity plugin type.
 */
abstract class CustomHeroEntityPluginBase extends CustomHeroPluginBase implements CustomHeroEntityPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    return $this->pluginDefinition['entity_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityBundle() {
    return $this->pluginDefinition['entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->configuration['entity'];
  }

}
