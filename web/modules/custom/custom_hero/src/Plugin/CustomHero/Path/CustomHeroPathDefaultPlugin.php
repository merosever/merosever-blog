<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Path;

/**
 * Default plugin which will be used if non of others met their requirements.
 *
 * @CustomHeroPath(
 *  id = "custom_hero_path_default",
 *  match_path = {"*"},
 *  weight = -100,
 * )
 */
class CustomHeroPathDefaultPlugin extends CustomHeroPathPluginBase {

}
