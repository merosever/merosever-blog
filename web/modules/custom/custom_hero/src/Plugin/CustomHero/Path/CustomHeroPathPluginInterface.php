<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Path;

use Drupal\custom_hero\Plugin\CustomHero\CustomHeroPluginInterface;

/**
 * Interface for CustomHero path plugin type.
 */
interface CustomHeroPathPluginInterface extends CustomHeroPluginInterface {

  /**
   * Gets match path.
   *
   * @return array
   *   An array with paths.
   */
  public function getMatchPath();

  /**
   * Gets match type.
   *
   * @return string
   *   An match type.
   */
  public function getMatchType();

}
