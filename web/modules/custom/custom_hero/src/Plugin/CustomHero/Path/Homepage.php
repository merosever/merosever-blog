<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Path;

use Drupal\custom_hero\Plugin\CustomHero\Path\CustomHeroPathPluginBase;
use Drupal\media\MediaInterface;

/**
 * Hero block for path.
 *
 * @CustomHeroPath(
 *   id = "homepage",
 *   match_type = "listed",
 *   match_path = {"/homepage"}
 * )
 */
class Homepage extends CustomHeroPathPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeroTitle() {
    return t('Web developer blog');
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroSubtitle() {

    $message = t('Hello! In this blog, I mainly write tutorials on web development using CMF Drupal and everything related to it. But sometimes, something else happens.');

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    /** @var \Drupal\media\MediaStorage $media_storage */
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_image = $media_storage->load(15);
    if ($media_image instanceof MediaInterface) {
      return $media_image->get('field_media_image')->entity->getFileUri();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroVideo() {
    /** @var \Drupal\media\MediaStorage $media_storage */
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_video = $media_storage->load(16);
    if ($media_video instanceof MediaInterface) {
      return [
        'video/mp4' => $media_video->get('field_media_video_file')->entity->getFileUri(),
      ];
    }
  }

}
