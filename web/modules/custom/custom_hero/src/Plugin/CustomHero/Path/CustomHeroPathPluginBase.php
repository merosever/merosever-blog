<?php

namespace Drupal\custom_hero\Plugin\CustomHero\Path;

use Drupal\custom_hero\Plugin\CustomHero\CustomHeroPluginBase;

/**
 * The base for CustomHero path plugin type.
 */
abstract class CustomHeroPathPluginBase extends CustomHeroPluginBase implements CustomHeroPathPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getMatchPath() {
    return $this->pluginDefinition['match_path'];
  }

  /**
   * {@inheritdoc}
   */
  public function getMatchType() {
    return $this->pluginDefinition['match_type'];
  }

}
