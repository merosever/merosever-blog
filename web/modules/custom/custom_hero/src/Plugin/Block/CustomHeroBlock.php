<?php

namespace Drupal\custom_hero\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\custom_hero\Plugin\CustomHeroPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Custom Hero' block.
 *
 * @Block(
 *   id = "custom_hero",
 *   admin_label = @Translation("Custom Hero"),
 *   category = @Translation("Custom")
 * )
 */
class CustomHeroBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The plugin manager for custom hero entity plugins.
   *
   * @var \Drupal\custom_hero\Plugin\CustomHeroPluginManager
   */
  protected $customHeroEntityManager;

  /**
   * The plugin manager for custom hero path plugins.
   *
   * @var \Drupal\custom_hero\Plugin\CustomHeroPluginManager
   */
  protected $customHeroPathManager;

  /**
   * Constructs a new CustomHeroBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\custom_hero\Plugin\CustomHeroPluginManager $custom_hero_entity
   *   The plugin manager for custom hero entity plugins.
   * @param \Drupal\custom_hero\Plugin\CustomHeroPluginManager $custom_hero_path
   *   The plugin manager for custom hero path plugins.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CustomHeroPluginManager $custom_hero_entity, CustomHeroPluginManager $custom_hero_path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->customHeroEntityManager = $custom_hero_entity;
    $this->customHeroPathManager = $custom_hero_path;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.custom_hero.entity'),
      $container->get('plugin.manager.custom_hero.path')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity_plugins = $this->customHeroEntityManager->getSuitablePlugins();
    $path_plugins = $this->customHeroPathManager->getSuitablePlugins();

    if (is_array($path_plugins) && is_array($entity_plugins)) {
      $plugins = $entity_plugins + $path_plugins;
      uasort($plugins, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
      $plugin = end($plugins);

      if ($plugin['plugin_type'] == 'entity') {
        /** @var \Drupal\custom_hero\Plugin\CustomHero\CustomHeroPluginInterface $instance */
        $instance = $this->customHeroEntityManager->createInstance($plugin['id'], ['entity' => $plugin['entity']]);
      }

      if ($plugin['plugin_type'] == 'path') {
        $instance = $this->customHeroPathManager->createInstance($plugin['id']);
      }

      $build['content'] = [
        '#theme' => 'custom_hero',
        '#title' => $instance->getHeroTitle(),
        '#subtitle' => $instance->getHeroSubtitle(),
        '#image' => $instance->getHeroImage(),
        '#video' => $instance->getHeroVideo(),
        '#plugin_id' => $instance->getPluginId(),
      ];
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [
      'url.path',
    ];
  }

}
