<?php

namespace Drupal\custom_hero\Generator;

use Drupal\Core\Entity\ContentEntityType;
use DrupalCodeGenerator\Command\BaseGenerator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Custom hero plugin generator for drush.
 */
class CustomHeroPluginGenerator extends BaseGenerator {

  /**
   * {@inheritdoc}
   */
  protected $name = 'plugin-custom-hero';

  /**
   * {@inheritdoc}
   */
  protected $alias = 'dpch';

  /**
   * {@inheritdoc}
   */
  protected $description = 'Generates CustomHero plugin.';

  /**
   * {@inheritdoc}
   */
  protected $templatePath = __DIR__ . '/templates';

  /**
   * The entity manager.
   */
  protected $entityTypeManager;

  /**
   * CustomHeroPluginGenerator constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, $name = NULL) {
    parent::__construct($name);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function interact(InputInterface $input, OutputInterface $output) {
    $questions = Utils::defaultPluginQuestions();
    $this->askForPluginType($questions);

    $questions['is_title'] = new ConfirmationQuestion(t('Do you want to custimize title?'), FALSE);
    $questions['is_subtitle'] = new ConfirmationQuestion(t('Do you want to add subtitle?'), FALSE);
    $questions['is_image'] = new ConfirmationQuestion(t('Do you want to specify image?'), FALSE);
    $questions['is_video'] = new ConfirmationQuestion(t('Do you want to specify video?'), FALSE);

    $vars = &$this->collectVars($input, $output, $questions);
    $vars['name'] = Utils::camelize($vars['plugin_id']);
    $vars['type'] = Utils::camelize($vars['custom_hero_plugin_type']);
    $vars['twig_template'] = 'custom-hero-' . $vars['custom_hero_plugin_type'] . '-plugin.html.twig';

    // Additional questions.
    if ($vars['custom_hero_plugin_type'] == 'path') {
      $questions['match_type'] = new ChoiceQuestion(t('Match type for path'), [
        'listed' => t('Only on listed page'),
        'unlisted' => t('All except listed'),
      ], 'listed');
    }

    if ($vars['custom_hero_plugin_type'] == 'entity') {
      $entity_types = [];
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
        if ($entity_type instanceof ContentEntityType) {
          $entity_types[$entity_type_id] = $entity_type->getLabel();
        }
      }
      $questions['entity_type'] = new ChoiceQuestion(t('Entity type'), $entity_types);
    }

    $vars = &$this->collectVars($input, $output, $questions, $vars);

    $this->addFile()
      ->path('src/Plugin/CustomHero/{type}/{name}.php')
      ->template($vars['twig_template']);
  }

  /**
   * Asks for preferred plugin type.
   */
  public function askForPluginType(&$question) {
    $custom_hero_plugins_types = [
      'path' => 'Custom Path plugin',
      'entity' => 'CustomHero Entity plguin',
    ];

    $question['custom_hero_plugin_type'] = new ChoiceQuestion(
      t('What plugin type do you want to create?'),
      $custom_hero_plugins_types
    );
  }

}
