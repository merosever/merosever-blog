<?php

/**
 * @file
 * Main file for all custom the,e hooks preprocess.
 */

/**
 * Implements template_preprocess_HOOK() for custom-hero.html.twig.
 */
function template_preprocess_custom_hero(&$variables) {
  // Image os required for video.It will be used for poster.
  if (!empty($variables['video'])) {
    if (!$variables['image']) {
      $variables['video'] = [];
    }
  }
}
