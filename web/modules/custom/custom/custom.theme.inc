<?php

/**
 * @file
 * Main file for preprocessing custom theme hooks.
 */

/**
 * Implements template_preprocess_HOOK() for blog-previous-next.html.twig.
 */
function template_preprocess_blog_previous_next(&$variable) {

  /** @var \Drupal\Core\Entity\EntityInterface $entity */
  $entity = $variable['entity'];
  $variable['next'] = [];
  $variable['previous'] = [];

  $entity_storage = \Drupal::entityTypeManager()
    ->getStorage($entity->getEntityTypeId());

  // Getting next entity ID.
  $next_entity_id = $entity_storage->getQuery()
    ->condition('type', $entity->bundle())
    ->condition('created', $entity->getCreatedTime(), '>')
    ->range(0, 1)
    ->sort('created', 'ASC')
    ->execute();

  if (!empty($next_entity_id)) {
    $next_entity = $entity_storage->load(reset($next_entity_id));

    $variable['next']['label'] = $next_entity->label();
    $variable['next']['url'] = $next_entity->toUrl()->toString(TRUE)->getGeneratedUrl();
  }

  // Getting previous entity ID.
  $previous_entity_id = $entity_storage->getQuery()
    ->condition('type', $entity->bundle())
    ->condition('created', $entity->getCreatedTime(), '<')
    ->range(0, 1)
    ->sort('created', 'DESC')
    ->execute();

  if (!empty($previous_entity_id)) {
    $previous_entity = $entity_storage->load(reset($previous_entity_id));

    $variable['previous']['label'] = $previous_entity->label();
    $variable['previous']['url'] = $previous_entity->toUrl()->toString(TRUE)->getGeneratedUrl();
  }

}
