<?php

namespace Drupal\custom\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\Messenger;

/**
 * Class SocialIcons.
 *
 * Create block with social links.
 *
 * @Block(
 *   id = "custom_block_social_links",
 *   admin_label = @Translation("Custom Blog - Social links"),
 * )
 */
class SocialIcons extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entity_type_manager;

  /**
   * Service messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  public $messenger;

  /**
   * Creates Social Icons block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Messenger $messenger) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entity_type_manager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * Link fields for social icons.
   *
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['field_group'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Information'),
    ];

    $form['field_group']['description'] = [
      '#type' => 'markup',
      '#markup' => t('If you leave the field blank, the link will not be displayed.'),
    ];

    $form['facebook_link'] = array(
      '#type' => 'url',
      '#title' => t('Facebook link'),
      '#description' => t('Add your link for facebook.'),
      '#default_value' => isset($config['facebook_link']) ? $config['facebook_link'] : '',
    );

    $form['telegram_link'] = array(
      '#type' => 'url',
      '#title' => t('Telegram link'),
      '#description' => t('Add your link for telegram.'),
      '#default_value' => isset($config['telegram_link']) ? $config['telegram_link'] : '',
    );

    $form['youtube_link'] = array(
      '#type' => 'url',
      '#title' => t('Youtube link'),
      '#description' => t('Add your link for youtube.'),
      '#default_value' => isset($config['youtube_link']) ? $config['youtube_link'] : '',
    );

    $form['twitter_link'] = array(
      '#type' => 'url',
      '#title' => t('Twitter link'),
      '#description' => t('Add your link for twitter.'),
      '#default_value' => isset($config['twitter_link']) ? $config['twitter_link'] : '',
    );

    $form['vk_link'] = array(
      '#type' => 'url',
      '#title' => t('VK link'),
      '#description' => t('Add your link for VK.'),
      '#default_value' => isset($config['vk_link']) ? $config['vk_link'] : '',
    );

    $form['github_link'] = array(
      '#type' => 'url',
      '#title' => t('GitHub link'),
      '#description' => t('Add your link for GitHub.'),
      '#default_value' => isset($config['github_link']) ? $config['github_link'] : '',
    );

    $form['gitlab_link'] = array(
      '#type' => 'url',
      '#title' => t('GitLab link'),
      '#description' => t('Add your link for GitLab.'),
      '#default_value' => isset($config['gitlab_link']) ? $config['gitlab_link'] : '',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    foreach ($config as $item => $value) {
      if (strpos($item, '_link')) {
        $name = stristr($item, '_link', TRUE);
        $social_values[$name] = $value;
      }
    }

    $block = [
      '#theme' => 'block_social_links',
      '#social_values' => $social_values,
    ];

    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    $form_values = $form_state->getValues();

    foreach ($form_values as $item => $value) {
      if (strpos($item, '_link')) {
        if (!is_string($value)) {
          $this->messenger->addMessage(t('The field cannot contain any values ​​except string.'), 'error');
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['facebook_link'] = $form_state->getValue('facebook_link');
    $this->configuration['telegram_link'] = $form_state->getValue('telegram_link');
    $this->configuration['youtube_link'] = $form_state->getValue('youtube_link');
    $this->configuration['twitter_link'] = $form_state->getValue('twitter_link');
    $this->configuration['vk_link'] = $form_state->getValue('vk_link');
    $this->configuration['github_link'] = $form_state->getValue('github_link');
    $this->configuration['gitlab_link'] = $form_state->getValue('gitlab_link');
  }

}
