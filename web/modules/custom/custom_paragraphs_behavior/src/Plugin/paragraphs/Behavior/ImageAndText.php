<?php

namespace Drupal\custom_paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class GalleryBehavior.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraphs_behavior_image_and_text",
 *   label = @Translation("Image and text settings"),
 *   description = @Translation("Settings for image and text paragrpah type."),
 *   weight = 0,
 * )
 */
class ImageAndText extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), ['p_image_and_text']);
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

    $class_name = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode);
    $image_position = $paragraph->getBehaviorSetting($this->getPluginId(), 'select_image_position');
    $image_size = $paragraph->getBehaviorSetting($this->getPluginId(), 'image_size');
    $build['#attributes']['class'][] = Html::getClass($class_name . '--image-size-' . $image_size . '-' . $image_position);
    $build['#attributes']['class'][] = Html::getClass($class_name);

    if (isset($build['field_image']) && $build['field_image']['#formatter']) {
      switch ($image_size) {
        case '4-12':
          $image_style = 'paragraph_image_text_4_of_12';
          break;

        case '6-12':
          $image_style = 'paragraph_image_text_6_of_12';
          break;

        case '8-12':
          $image_style = 'paragraph_image_text_8_of_12';
          break;

        default:
          $image_style = 'paragraph_image_text_4_of_12';
      }
      $build['field_image'][0]['#image_style'] = $image_style;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['select_image_position'] = [
      '#type' => 'select',
      '#title' => $this->t('Image position'),
      '#description' => $this->t('Select image position.'),
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'select_display_side', 'left'),
    ];

    $form['image_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Image size'),
      '#description' => $this->t('Size of the image in grid.'),
      '#options' => [
        '4-12' => $this->t('4 column of 12'),
        '6-12' => $this->t('6 column of 12'),
        '8-12' => $this->t('8 column of 12'),
      ],
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'image_size', '6_12'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $behavior_settings = $paragraph->getAllBehaviorSettings();
    if (!empty($behavior_settings['custom_paragraphs_behavior_image_and_text'])) {
      foreach ($behavior_settings['custom_paragraphs_behavior_image_and_text'] as $setting_name => $setting) {
        if ($setting_name == 'select_image_position' && !empty($setting)) {
          $info[] = $this->t('Current image display side:@position', ['@position' => $setting]);
        }
        if ($setting_name == 'image_size' && !empty($setting)) {
          $info[] = $this->t('Image size selected:@size', ['@size' => $setting]);
        }
      }
    }

    return !empty($info) ? $info : '';
  }

}
