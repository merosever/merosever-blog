<?php

namespace Drupal\custom_paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class GalleryBehavior.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraphs_styles_behavior",
 *   label = @Translation("Custom styles for paragraph"),
 *   description = @Translation("Allows you to change the style settings for a paragraph."),
 *   weight = 0,
 * )
 */
class StylesBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $bem_block = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode);
    $classes = $paragraph->getBehaviorSetting($this->getPluginId(), 'styles', []);
    foreach ($classes as $class) {
      $build['#attributes']['class'][] = $bem_block . '--' . Html::getClass($class);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['style_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Paragraphs styles'),
      '#open' => FALSE,
    ];

    $styles = $this->getStyles($paragraph);
    $selected_styles = $paragraph->getBehaviorSetting($this->getPluginId(), 'styles');

    foreach ($styles as $group_id => $group) {
      $form['style_wrapper'][$group_id] = [
        '#type' => 'checkboxes',
        '#title' => $group['label'],
        '#options' => $group['options'],
      ];

      if (!empty($selected_styles)) {
        $form['style_wrapper'][$group_id]['#default_value'] = $selected_styles;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $behavior_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'styles');
    $all_styles = !empty($behavior_settings) ? implode(",", $behavior_settings) : '';

    return $all_styles ? [$this->t('All styles: @style', ['@style' => $all_styles])] : '';
  }

  /**
   * {@inheritdoc}
   */
  public function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $styles = [];
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph, $form, $form_state);

    if (!empty($filtered_values['style_wrapper'])) {
      foreach ($filtered_values['style_wrapper'] as $group) {
        foreach ($group as $style_name) {
          $styles[] = $style_name;
        }
      }
    }

    $paragraph->setBehaviorSettings($this->getPluginId(), ['styles' => $styles]);
  }

  /**
   * Return styles for paragraph.
   */
  public function getStyles(ParagraphInterface $paragraph) {
    $styles = [];

    if ($paragraph->hasField('field_title')) {
      $styles['title'] = [
        'label' => $this->t('Paragraphs title'),
        'options' => [
          'title_bold' => $this->t('Bold'),
          'title_centered' => $this->t('Centered'),
        ],
      ];
    }

    $styles['common'] = [
      'label' => $this->t('Paragraph common styles'),
      'options' => [
        'style_black' => $this->t('Style black'),
      ],
    ];

    return $styles;
  }

}
