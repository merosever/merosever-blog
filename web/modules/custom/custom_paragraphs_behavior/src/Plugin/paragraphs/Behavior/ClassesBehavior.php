<?php

namespace Drupal\custom_paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class GalleryBehavior.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraphs_classes_behavior",
 *   label = @Translation("Custom classes for paragraph"),
 *   description = @Translation("Allows to add custom classes for paragraph."),
 *   weight = 0,
 * )
 */
class ClassesBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $behavior_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_class', '');
    $classes = explode(' ', $behavior_settings);
    foreach ($classes as $class) {
      $build['#attributes']['class'][] = Html::getClass($class);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['custom_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#description' => $this->t('Multiple classes separated by space.They will be processed via Html::getClass().'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_class', ''),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $behavior_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'custom_class');
    return $behavior_settings ? [$this->t('All classes: @class', ['@class' => $behavior_settings])] : '';
  }

}
