<?php

namespace Drupal\custom_paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class GalleryBehavior.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraphs_behavior_gallery",
 *   label = @Translation("Gallery settings"),
 *   description = @Translation("Settings for gallery paragrpah type."),
 *   weight = 0,
 * )
 */
class GalleryBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return in_array($paragraphs_type->id(), ['p_gallery', 'p_gallery_youtube']);
  }

  /**
   * Extends the paragraph render array with behavior.
   *
   * @param array &$build
   *   A renderable array representing the paragraph. The module may add
   *   elements to $build prior to rendering. The structure of $build is a
   *   renderable array as expected by drupal_render().
   * @param \Drupal\paragraphs\Entity\Paragraph $paragraph
   *   The paragraph.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
   *   The entity view display holding the display options configured for the
   *   entity components.
   * @param string $view_mode
   *   The view mode the entity is rendered in.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

    $select_number_items_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'select_number_items', 3);
    $class_name = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode) . '--item-per-row-' . $select_number_items_settings;
    $build['#attributes']['class'][] = Html::getClass($class_name);

    // Only Gallery images.
    if ($paragraph->getType() == 'p_gallery') {

      $select_number_items_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'select_number_items', 3);
      $class_name = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode) . '--item-per-row-' . $select_number_items_settings;
      $build['#attributes']['class'][] = Html::getClass($class_name);

      if (isset($build['field_images']) && $build['field_images']['#formatter'] == 'photoswipe_field_formatter') {
        switch ($select_number_items_settings) {
          case 4:
            $image_style = 'paragraph_gallery_image_3_of_12';
            break;
          case 3:
            $image_style = 'paragraph_gallery_image_4_of_12';
            break;
          case 2:
            $image_style = 'paragraph_gallery_image_6_of_12';
            break;
          default:
            $image_style = '';
            break;
        }
      }

      for($i = 0;$i < count($build['field_images']['#items']);$i++) {
        $build['field_images'][$i]['#display_settings']['photoswipe_node_style'] = $image_style;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->getType() == 'p_gallery_youtube') {
      $form['video_width'] = [
        '#type' => 'select',
        '#title' => $this->t('Video width'),
        '#description' => $this->t('Maximum width for video.'),
        '#options' => [
          'full' => '100%',
          '720px' => '720px',
        ],
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'video_width', 'full'),
      ];
    }

    $form['select_number_items'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of items displayed'),
      '#description' => $this->t('Specify the number of items displayed.'),
      '#options' => [
        '2' => $this->formatPlural(2, '1 element per row', '@count elements per row'),
        '3' => $this->formatPlural(3, '1 element per row', '@count elements per row'),
        '4' => $this->formatPlural(4, '1 element per row', '@count elements per row'),
      ],
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'select_number_items', 3),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $behavior_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'select_number_items');
    return $behavior_settings ? [$this->t('Number of items displayed: @element', ['@element' => $behavior_settings])] : '';
  }

}
