<?php

namespace Drupal\custom_paragraphs_behavior\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class GalleryBehavior.
 *
 * @ParagraphsBehavior(
 *   id = "custom_paragraphs_title_behavior",
 *   label = @Translation("Paragraph title settings"),
 *   description = @Translation("Allows to configure title for paragraph."),
 *   weight = 0,
 * )
 */
class TitleBehavior extends ParagraphsBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * Extends the paragraph render array with behavior.
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

  }

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables) {
    /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
    $paragraph = $variables['paragraph'];
    $variables['title_wrapper'] = $paragraph->getBehaviorSetting($this->getPluginId(), 'title_wrapper', 'h2');
  }

  /**
   * {@inheritdoc}
   */
  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    if ($paragraph->hasField('field_title')) {
      $form['title_wrapper'] = [
        '#type' => 'select',
        '#title' => $this->t('Title settings'),
        '#description' => $this->t('Allows to configure title for paragraph.'),
        '#options' => [
          'h1' => $this->t('h1'),
          'h2' => $this->t('h2'),
          'h3' => $this->t('h3'),
          'h4' => $this->t('h4'),
          'h5' => $this->t('h5'),
          'h6' => $this->t('h6'),
          'div' => $this->t('div'),
        ],
        '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId(), 'title_wrapper', 'h2'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(Paragraph $paragraph) {
    $behavior_settings = $paragraph->getBehaviorSetting($this->getPluginId(), 'title_wrapper');
    return $behavior_settings ? [$this->t('Title setting: @title', ['@title' => $behavior_settings])] : '';
  }

}
