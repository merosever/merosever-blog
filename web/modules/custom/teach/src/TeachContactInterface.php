<?php

namespace Drupal\teach;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Teach Contact Entity.
 */
interface TeachContactInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface{

}